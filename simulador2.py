#coding: utf-8

from random import uniform, choices
import simpy, sys
from servidor import DataCenter

listaProbalidadeChegada = [35,19,19,13,3,7,1,2,1]

def cliente(idCliente, simulador, dataCenter, tempoChegadaFila):
    """ Função para fazer o atendimento e a escolha do recurso """

    with dataCenter.servidores.request() as servidor:
        yield servidor
        tempoInicial = simulador.now
        dataCenter.listaUsuariosFila.append(len(dataCenter.servidores.queue))
        id_serv = dataCenter.atribuiProbabilidades(dataCenter.servidores)
        print('Cliente %d entrou no servidor %d no tempo %d' % (idCliente, id_serv, simulador.now))
        yield simulador.timeout(geraTempo(dataCenter.probabilidades, 1, 10, 9.5, 0.5))
        print('Cliente %d saiu do servidor %d no tempo %d' % (idCliente, id_serv, simulador.now))
        dataCenter.libera(id_serv)
        tempoFinal = simulador.now
        tempoAtendimento = tempoFinal - tempoInicial
        tempoFila = tempoInicial - tempoChegadaFila
        tempoSistema = tempoFinal - tempoChegadaFila
        dataCenter.listaTempoAtendimento.append(tempoAtendimento)
        dataCenter.listaTempoFila.append(tempoFila)
        dataCenter.listaTempoSistema.append(tempoSistema)
        print('Servidor %d liberado!!' % id_serv)

def geraListaAleatorio(inicio, fim, primeiroTermo, razao):
    """" Função para gerar a lista de probabilidades aleatórias através de uma PA (progressão aritmética) """
    lista = []
    for n in range(inicio, fim):
        x = primeiroTermo + (n - 1) * razao
        lista.append(uniform(x, x + razao))
    return lista

def geraTempo(listaProbalidades, inicio, fim, primeiroTermo, razao):
    """ Função para gerar os tempo de entrada e de serviço """
    aleatorioClasse = geraListaAleatorio(inicio, fim, primeiroTermo, razao)
    return choices(aleatorioClasse, listaProbalidades, k=1)[0]

def geraCliente(simulador, dataCenter, nClientes):
    """ Função para gerar os clientes e entrar na simulação """
    print ('='*150)
    #nClientes = 10000
    for idCliente in range(nClientes):	
        tempoChegadaFila = simulador.now
        simulador.process(cliente(idCliente, simulador, dataCenter, tempoChegadaFila))        
        print('Cliente %d chegou na fila no tempo %d' % (idCliente, tempoChegadaFila))
        yield simulador.timeout(geraTempo(listaProbalidadeChegada, 1, 10, 0, 5))
    return simulador.now

nClientes = 10000
simulador = simpy.Environment()
dataCenter = DataCenter(simulador)
clientes = simulador.process(geraCliente(simulador, dataCenter, nClientes))
simulador.run(clientes)

print('\n\n')
print('*'*17, 'Análise', '*'*17)
print('Total de clientes no servidor 1:', dataCenter.totalClientesServidor1)
print('Total de clientes no servidor 2:', dataCenter.totalClientesServidor2)

percentualServidor1 = (dataCenter.totalClientesServidor1 * 100) / nClientes
percentualServidor2 = (dataCenter.totalClientesServidor2 * 100) / nClientes

print('Percentual de clientes no servidor 1: %.2f' % percentualServidor1)
print('Percentual de clientes no servidor 2: %.2f' % percentualServidor2)

numClientesFila = dataCenter.calculaMedia(dataCenter.listaUsuariosFila)
numMedioAtendimento = dataCenter.calculaMedia(dataCenter.listaTempoAtendimento)
tempoMedioFila = dataCenter.calculaMedia(dataCenter.listaTempoFila)
tempoMedioSistema = dataCenter.calculaMedia(dataCenter.listaTempoSistema)

print('Número médio de clientes na fila: %.2f' % numClientesFila)
print('Taxa média de ocupação: %.2f' % numMedioAtendimento)
print('Tempo médio de um cliente na fila: %.2f' % tempoMedioFila)
print('Tempo médio de um cliente no sistema: %.2f' % tempoMedioSistema)
