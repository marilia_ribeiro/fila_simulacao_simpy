#coding: utf-8

from random import randrange, choices
import simpy

from servidor import Servidor

class Cliente(object):
    def __init__(self, id, simulador, servidores):
        self.id = id
        self.simulador = simulador
        self.acao = simulador.process(self.run(servidores))
        self.listaProbalidadeChegada = [35,19,19,13,3,7,1,2,1]
        self.aleatorioClasse = []
        self.chegada = self.tempoChegada()
        self.tempoServidor = 0
    
    def run(self, servidores):
        while True:
            yield self.simulador.timeout(self.chegada)
            print('Chegou na fila no tempo %d' % self.simulador.now)

            with servidores.request() as servidor:
                yield servidor
                # gerar tempo no servidor
    
    def geraListaAleatorio(self, inicio, fim, primeiroTermo, razao, lista):
        for n in range(inicio, fim):
             x = primeiroTermo + (n - 1) * razao
             lista.append(randrange(x, x + razao))
        return lista

    def tempoChegada(self):
        self.aleatorioClasse = self.geraListaAleatorio(1, 10, 0, 5, self.aleatorioClasse)
        return choices(self.aleatorioClasse, self.listaProbalidadeChegada, k=1)[0]

# def atendimento(simulador):
#     i = 1
#     while True:
#         print('Start parking at %d' % simulador.now)
#         parking_duration = 5
#         yield simulador.timeout(parking_duration)
#         print('Start driving at %d' % simulador.now)
#         trip_duration = 2
#         yield simulador.timeout(trip_duration)

if __name__ == "__main__":
    nClientes = 10
    # clientes = [ Cliente() for x in range(0, nClientes) ]
    # servidor1 = Servidor([6,5,23,20,21,12,9,2,1])
    # servidor2 = Servidor([5,4,15,16,23,20,10,5,2])
    simulador = simpy.Environment()
    servidores = simpy.Resource(simulador, capacity=2)
    for idCliente in range(nClientes):
        cliente = Cliente(idCliente, simulador, servidores)
        simulador.process(cliente)
    # simulador.process(atendimento(simulador))
    # simulador.run(until=nClientes)

    # print(filaClientes[0].aleatorioClasse)
    # print(filaClientes[0].chegada)
    # print(filaClientes[0].listaProbalidadeChegada)



