Para executar esse código é necessário utilizar o python 3.6.

- Caso você não tenha o python 3.6 instalado, realize as seguintes etapas:

sudo add-apt-repository ppa:jonathonf/python-3.6

sudo apt-get update

sudo apt-get install python3.6



- Crie uma virtualenv com o python 3.6 utilizando o virtualenvwrapper

mkvirtualenv --python=/usr/bin/python3.6 simpy

- Instale os requirements

pip install -r requirements.txt


- Execute o código:

python simulador2.py
