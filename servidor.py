#coding: utf-8
import simpy
import numpy as np

class DataCenter(object):
    def __init__(self, simulador):
        self.servidores = simpy.Resource(simulador, capacity=2)
        self.listaUsuariosFila = []
        self.listaTempoAtendimento = []
        self.listaTempoFila = []
        self.listaTempoSistema = []
        self.servidor1 = [6,5,23,20,21,12,9,2,1]
        self.servidor2 = [5,4,15,16,23,20,10,5,2]
        self.probabilidades = self.servidor1
        self.totalClientesServidor1 = 0
        self.totalClientesServidor2 = 0
        self.used = [False, False]

    def calculaMedia(self, lista):
        """ Calcula média de uma lista """
        return np.mean(lista)
    
    def atribuiProbabilidades(self, servidores):
        """ Verifica qual servidor está sendo usado, para atribuir corretamente cada probabilidade """
        servidorUsado = None
        if (servidores.count == 1):
            self.probabilidades = self.servidor1
            self.totalClientesServidor1 += 1
            servidorUsado, self.used[0] = 0, True
        else:
            if not self.used[0]:
                servidorUsado = 0
                self.probabilidades = self.servidor1
                self.totalClientesServidor1 += 1
            else:
                servidorUsado = 1
                self.probabilidades = self.servidor2
                self.totalClientesServidor2 += 1
            self.used[servidorUsado] = True
        return servidorUsado
    
    def libera(self, idServidor):
        """ Libera o servidor. Controle utilizado para poder atribuir as probabilidades """
        self.used[idServidor] = False
        
